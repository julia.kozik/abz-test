import React, { useState } from "react";
import { useForm } from "react-hook-form";
import Button from "./components/Button/Button";
import Logo from "./components/Logo/Logo";
import InputText from "./components/InputText/InputText";
import "./App.css";
import Card from "./components/Card/Card";
import RadioButtons from "./components/Radio-Buttons/RadioButtons";
import InputFile from "./components/Input-file/InputFile";
import Preloader from "./components/Preloader/Preloader";
import { usePositions } from "./apiClient/getUsersPositions";
import { useUsers } from "./apiClient/hookUsers";

const EMAIL_PATTERN =
  /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

const PHONE_PATTERN = /^\+380[0-9]{9}/;

function App() {
  const {
    register,
    watch,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm();

  const { positions, isLoadingPositions, positionsFetchError } = usePositions();

  const {
    users,
    isLoading: isLoadingUsers,
    error: usersFetchError,
    hasMore,
    nextPage,
    resetUsersState,
  } = useUsers();

  const [errorSubmit, setErrorSubmit] = useState<string>("");
  const [isSubmitting, setIsSubmitting] = useState(false);

  const onSubmit2 = async (data: any) => {
    try {
      setIsSubmitting(true);
      const formData = new FormData();
      formData.append("name", data.name);
      formData.append("email", data.email);
      formData.append("phone", data.phone);
      formData.append("position_id", data.position_id);
      formData.append("photo", data.photo[0]);

      const tokenResponse = await fetch(
        "https://frontend-test-assignment-api.abz.agency/api/v1/token"
      );
      const tokenResponseJson = await tokenResponse.json();
      const addUserResponse = await fetch(
        "https://frontend-test-assignment-api.abz.agency/api/v1/users",
        {
          method: "POST",
          body: formData,
          headers: {
            Token: tokenResponseJson.token,
          },
        }
      );

      const addUserResponseJson = await addUserResponse.json();

      if (addUserResponseJson.success) {
        onAddUserSuccess();
      } else {
        setErrorSubmit(addUserResponseJson.message);
      }

      // if success
    } catch (error) {
      setErrorSubmit("User registration failed.");
    } finally {
      setIsSubmitting(false);
    }
  };

  const onAddUserSuccess = () => {
    // Reset form
    reset();
    // Refresh users
    resetUsersState();
    // scroll to the users
    window.location.replace("#user-list");
  };

  return (
    <>
      <header className="full-width top-bar">
        <div className="container flex">
          <Logo />

          <div className="navigation flex">
            <Button as="a" href="#user-list">
              Users
            </Button>

            <Button as="a" href="#sign-up-form">
              Sign Up
            </Button>
          </div>
        </div>
      </header>

      <main className="full-width">
        <div className="hero center">
          <div className="container ">
            <div className="hero-content body-16">
              <h1>Test assignment for front-end developer</h1>
              
              <p>
                What defines a good front-end developer is one that has skilled
                knowledge of HTML, CSS, JS with a vast understanding of User
                design thinking as they'll be building web interfaces with
                accessibility in mind. They should also be excited to learn, as
                the world of Front-End Development keeps evolving.
              </p>

              <Button as="a" href="#sign-up-form">
                Sign Up
              </Button>
            </div>
          </div>
        </div>

        {/* Cards */}
        <div id="user-list" className="cards container column">
          <h1 className="title">Working with GET request</h1>

          <div className="grid">
            {users &&
              users.map((user, idx) => (
                <Card
                  key={`${user.id}-${idx}`}
                  userImageSrc={user.photo}
                  userImageAlt={user.name}
                  userImageFallback={user.name.slice(0,1)} // Use  the first letter as a fallback
                  userName={user.name}
                  occupation={user.position}
                  email={user.email}
                  phone={user.phone}
                ></Card>
              ))}
          </div>

          {isLoadingUsers && <Preloader />}

          {usersFetchError && "Unable to get users."}

          {hasMore && !isLoadingUsers && (
            <Button as="button" className="cards-more" onClick={nextPage}>
              Show More
            </Button>
          )}
        </div>

        <div id="sign-up-form" className="form-container container">
          <h1 className="title">Working with POST request</h1>

          <form className="application-form" onSubmit={handleSubmit(onSubmit2)}>
            <InputText
              id="user-name-field"
              register={register}
              name="name"
              label="Your Name"
              value={watch("name")}
              validationRules={{
                minLength: { value: 2, message: "Minimum 2 characters" },
                maxLength: { value: 60, message: "Maximum 60 characters" },
                required: { value: true, message: "Field is required" },
              }}
              errorMessage={errors.name?.message as string}
            />

            <InputText
              id="user-email-field"
              register={register}
              name="email"
              label="Email"
              value={watch("email")}
              validationRules={{
                required: { value: true, message: "Field is required" },
                pattern: {
                  value: EMAIL_PATTERN,
                  message: "Must be valid email",
                },
              }}
              errorMessage={errors.email?.message as string}
            />

            <InputText
              id="user-phone-field"
              register={register}
              name="phone"
              label="Phone"
              value={watch("phone")}
              validationRules={{
                required: { value: true, message: "Field is required" },
                pattern: {
                  value: PHONE_PATTERN,
                  message: "Must be valid Ukrainian number",
                },
              }}
              errorMessage={errors.phone?.message as string}
            />

            {isLoadingPositions && <Preloader />}

            {!isLoadingPositions && positions.length && !positionsFetchError ? (
              <RadioButtons
                id="user-position-field"
                options={positions}
                name="position_id"
                register={register}
              />
            ) : (
              <p style={{ color: "red" }}>Unable to fetch positions</p>
            )}

            <InputFile
              id="user-image"
              name="photo"
              register={register}
              files={watch("photo")}
              accept="image/jpeg"
              validationRules={{
                validate: {
                  size: (v: FileList) => v[0]?.size <= 5242880 || "Maximum 5mb",
                },
              }}
              errorMessage={errors.phone?.message as string}
            />

            {errorSubmit && <p className="error-message">{errorSubmit}</p>}

            {isSubmitting ? (
              <Preloader />
            ) : (
              <Button as="button" className="">
                Sign Up
              </Button>
            )}
          </form>
        </div>
      </main>
    </>
  );
}

export default App;
