import { useState, useEffect, useRef } from "react";

interface Links {
  next_url: string | null;
  prev_url: string | null;
}

interface User {
  id: string;
  name: string;
  email: string;
  phone: string;
  position: string;
  position_id: string;
  registration_timestamp: number;
  photo: string;
}

interface UsersResponse {
  success: boolean;
  page: number;
  total_pages: number;
  total_users: number;
  count: number;
  links: Links;
  users: User[];
}

export const useUsers = () => {
  const [users, setUsers] = useState<User[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [page, setPage] = useState(1);
  const [hasMore, setHasMore] = useState(false);

  const lastLoadedPage = useRef();

  useEffect(() => {
    if (page !== lastLoadedPage.current) {
      fetchUsers(page);
    }
  }, [page]);

  const fetchUsers = (page: number) => {
    setIsLoading(true);

    fetch(
      `https://frontend-test-assignment-api.abz.agency/api/v1/users?page=${page}&count=6`
    )
      .then((response) => response.json())
      .then((json) => {
        lastLoadedPage.current = json.page;
        setUsers((prev) => [...prev, ...json.users]);
        setHasMore(page !== json.total_pages);
        setError(false);
      })
      .catch(() => {
        setError(true);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const nextPage = () => {
    setPage((prev) => prev + 1);
  };

  const resetUsersState = () => {
    // reset state
    setUsers([]);
    setPage(1);
    setHasMore(false);
    // refetch data
    fetchUsers(1);
  };

  return { users, isLoading, error, hasMore, nextPage, resetUsersState };
};
