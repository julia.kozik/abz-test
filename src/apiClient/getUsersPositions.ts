import { useEffect, useState } from "react";

export const usePositions = () => {
  const [isLoadingPositions, setIsLoadingPositions] = useState(false);
  const [positions, setPositions] = useState([]);
  const [positionsFetchError, setPositionsFetchError] = useState(false);

  useEffect(() => {
    setIsLoadingPositions(true);
    fetch("https://frontend-test-assignment-api.abz.agency/api/v1/positions")
      .then((data) => data.json())
      .then((json) => {
        // console.log(json)
        if (Array.isArray(json?.positions)) {
          const mappedPositions = json.positions.map((position: any) => ({
            name: position.name,
            value: position.id
          }));
          setPositions(mappedPositions);
          setPositionsFetchError(false);
        }
      })
      .catch(() => {
       
        setIsLoadingPositions(true);
      })
      .finally(() => {
        setIsLoadingPositions(false);
      });
  }, []);

  return { isLoadingPositions, positions, positionsFetchError }
}
