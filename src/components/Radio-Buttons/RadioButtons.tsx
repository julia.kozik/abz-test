import React from "react";
import { UseFormRegister } from "react-hook-form";

import "./RadioButtons.css";

export interface Option {
    name:string;
    value: number | string;
}

interface RadioButtonsProps {
  id: string;
  options: Option[];
  required?: boolean;
  name: string;
  ariaLabel?: string;
  register: UseFormRegister<any>;
}

const RadioButtons = ({
  id,
  options,
  name,
  ariaLabel,
  register,
}: RadioButtonsProps) => (
  <div className="radio-group" aria-label={ariaLabel}>
    {options.map((option, idx) => (
      <div key={idx} className="option-line">
        <label className="form-control body-16" htmlFor={`${id}-${idx}`}>
          <input
            type="radio"
            className="radio-input"
            value={option.value}
            id={`${id}-${idx}`}
            {...register(name)}
          />
          {option.name}
        </label>
      </div>
    ))}
  </div>
);

export default RadioButtons;
