import React, { ReactNode } from "react";
import clsx from "clsx";
import "./Button.css";

interface BaseProps {
  className?: string;
  onClick?: () => void;
  children?: ReactNode;
  [key: string]: any;
}

type VariantProps =
  | {
      as: "button";
      onClick?: () => void;
    }
  | {
      as: "a";
      href: string;
    };

type ButtonProps = BaseProps & VariantProps;

function Button({className,children, as, ...rest}: ButtonProps) {
  const Tag = as;
  return (
    <Tag className={clsx("button body-16", className)} {...rest}>
      {children}
    </Tag>
  );
}

export default Button;
