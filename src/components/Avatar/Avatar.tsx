import React from "react";
import * as AvatarBase from "@radix-ui/react-avatar";
import "./Avatar.css";

interface AvatarProps {
  src?: string;
  alt: string;
  fallback: string;
}

const Avatar = ({ src, alt, fallback }: AvatarProps) => (
  <AvatarBase.Root className="avatar-root">
    <AvatarBase.Image className="avatar-image" src={src} alt={alt} />
    <AvatarBase.Fallback className="avatar-fallback body-16" delayMs={600}>
      {fallback}
    </AvatarBase.Fallback>
  </AvatarBase.Root>
);

export default Avatar;
