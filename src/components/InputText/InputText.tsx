import React from "react";
import { UseFormRegister } from "react-hook-form";
import clsx from "clsx";
import "./InputText.css";

interface InputProps extends React.HTMLProps<HTMLInputElement> {
  id: string;
  label: string;
  name: string;
  value: string;
  register: UseFormRegister<any>;
  validationRules?: any;
  errorMessage?: string;
  required?: boolean;
}

function InputText({
  id,
  label,
  name,
  value,
  register,
  validationRules,
  errorMessage,
  required,
  ...rest
}: InputProps) {
  return (
    <div>
      <div className={clsx("input-field", { error: errorMessage })}>
        <label
          htmlFor={id}
          className={clsx("input-label body-16", {
            filled: value,
            "body-12": value,
          })}
        >
          {label}
        </label>
        <input
          {...rest}
          {...register(name, validationRules)}
          className="input-text body-16"
          type="text"
          id={id}
          name={name}
          placeholder={label}
        />
      </div>
      {errorMessage && <p className="error-message body-12">{errorMessage}</p>}
    </div>
  );
}

export default InputText;
