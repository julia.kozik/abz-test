import React, { ReactNode } from "react";
import * as Tooltip from "@radix-ui/react-tooltip";

import "./Tooltip.css";

interface TooltipMessageProps {
  message: string;
  children: ReactNode;
}

const TooltipMessage = ({ message, children }: TooltipMessageProps) => {
  return (
    <Tooltip.Provider>
      <Tooltip.Root>
        <Tooltip.Trigger className="pointer" asChild>
          {children}
        </Tooltip.Trigger>
        <Tooltip.Portal>
          <Tooltip.Content className="TooltipContent body-16">
            {message}
          </Tooltip.Content>
        </Tooltip.Portal>
      </Tooltip.Root>
    </Tooltip.Provider>
  );
};

export default TooltipMessage;
