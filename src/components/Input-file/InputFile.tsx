import React, { KeyboardEventHandler, useEffect } from "react";
import clsx from "clsx";
import { UseFormRegister } from "react-hook-form";
import "./InputFile.css";

interface InputProps extends React.HTMLProps<HTMLInputElement> {
  id: string;
  name: string;
  register: UseFormRegister<any>;
  errorMessage?: string;
  files?: FileList;
  required?: boolean;
  validationRules?:any;
}

function InputFile({
  id,
  name,
  register,
  required,
  files,
  errorMessage,
  validationRules,
  ...rest
}: InputProps) {
  const fileName = files && files.length && files[0].name;

  const handleKeyDown: KeyboardEventHandler<HTMLLabelElement> = (event) => {
    if (event.key === "Enter" || event.key === " ") {
      document.getElementById(id)?.click();
    }
  };

  return (
    <div>
      <label
        htmlFor={id}
        className={clsx("file-uploader", { error: errorMessage })}
        tabIndex={0}
        onKeyDown={handleKeyDown}
      >
        <i className="upload-button body-16">upload</i>
        <i className={clsx("upload-input body-16", { filled: fileName })}>
          {fileName || "Upload your photo"}
        </i>
        <input {...rest} id={id} type="file" {...register(name,validationRules)} />
      </label>
      {errorMessage && <p className="error-message body-12">{errorMessage}</p>}
    </div>
  );
}

export default InputFile;
