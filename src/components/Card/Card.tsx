import React from "react";
import "./Card.css";
import Avatar from "../Avatar/Avatar";
import TooltipMessage from "../Tooltip/Tooltip";

interface CardProps {
  userImageSrc: string;
  userImageAlt: string;
  userImageFallback: string;
  userName: string;
  occupation: string;
  email: string;
  phone: string;
}

function Card({
  userImageSrc,
  userImageAlt,
  userImageFallback,
  userName,
  occupation,
  email,
  phone,
}: CardProps) {
  return (
    <div className="card-body body-16">
      <div className="user-img">
        <Avatar
          src={userImageSrc}
          alt={userImageAlt}
          fallback={userImageFallback}
        />
      </div>
      <TooltipMessage message={userName}>
        <p className="text-ellipsis user-name">{userName}</p>
      </TooltipMessage>
      <TooltipMessage message={occupation}>
        <p className="text-ellipsis">{occupation}</p>
      </TooltipMessage>
      <TooltipMessage message={email}>
        <p className="text-ellipsis">{email}</p>
      </TooltipMessage>
      <TooltipMessage message={phone}>
        <p className="text-ellipsis">{phone}</p>
      </TooltipMessage>
    </div>
  );
}

export default Card;
